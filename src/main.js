import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue'
import App from './App.vue'

import ClassicEditor from '@ckeditor/ckeditor5-build-classic'
import VueCkeditor from 'vue-ckeditor5'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

import store from './store'
import router from './routes'

const options = {
  editors: {
    classic: ClassicEditor,
  },
  name: 'ckeditor'
}

Vue.use(VueCkeditor.plugin, options);

Vue.config.productionTip = false

Vue.use(BootstrapVue);

new Vue({
  store,
  router,
  render: h => h(App)
}).$mount('#app')
