import moment from 'moment'

export const ProblemMixin = {
  filters: {
    excerpt(text) {
      console.log(stripHtml(text)); //eslint-disable-line
      return stripHtml(text).substr(0, 100);
    },
    created(date) {
      console.log(moment(date)); //eslint-disable-line
      let dateCheck = moment(date).from(moment());
      if(dateCheck.includes('day') || dateCheck.includes('year')) {
        return moment(date).format('DD-MM-YYYY HH:mm:ss');
      } else {
        return moment(date).from(moment());
      }      
    }
  }
}

function stripHtml(html){
  // Create a new div element
  var temporalDivElement = document.createElement("div");
  // Set the HTML content with the providen
  temporalDivElement.innerHTML = html;
  // Retrieve the text property of the element (cross-browser support)
  return temporalDivElement.textContent || temporalDivElement.innerText || "";
}