import { authService } from './../services/AuthService';

export function requiresAuth(to) {
    if (to.matched.some(record => record.meta.requiresAuth)) {
        if (!authService.isAuthenticated()) {
            return (window.location.href = '/login');
        }
        authService.setAxiosDefaultAuthorizationHeader();
    }
    return Promise.resolve(to);
}

export function requiresAdmin(to) {
    if(to.matched.some(record => record.meta.requiresAdmin)) {
        if (!authService.isAdmin() && !authService.isAuthenticated()) {
					return (window.location.href = '/login');
        } else if(!authService.isAdmin() && authService.isAuthenticated()) {
					return (window.location.href = '/');
				}
        authService.setAxiosDefaultAuthorizationHeader();
    }
		return Promise.resolve(to);
}

export function questOnly(to) {
	if(to.matched.some(record => record.meta.questOnly) && authService.isAuthenticated()) {
		return Promise.reject({ name: 'index' });
	}
	return Promise.resolve(to);
}