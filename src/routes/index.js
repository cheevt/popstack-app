import Vue from 'vue';
import VueRouter from 'vue-router';

import Index from './../pages/Index.vue';
import Login from './../pages/Login.vue';

import IndexAdmin from './../pages/admin/Index.vue';
import ProblemsAdmin from './../pages/admin/Problems.vue';
import ProblemAdd from './../pages/admin/problems/Add.vue';
import ProblemEdit from './../pages/admin/problems/Edit.vue';
import CategoriesAdmin from './../pages/admin/Categories.vue';
import CategoryAdd from './../pages/admin/categories/Add.vue';
import UsersAdmin from './../pages/admin/Users.vue';
import UserAdd from './../pages/admin/users/Add.vue';
import UserEdit from './../pages/admin/users/Edit.vue';

import SingleProblem from './../pages/SingleProblem.vue';
import Profile from './../pages/Profile.vue';
import ProfileProblems from './../pages/profile/Problems.vue';
import ProfileComments from './../pages/profile/Comments.vue';
import ProfileSettings from './../pages/profile/Settings.vue';

import { requiresAuth, requiresAdmin, questOnly } from './guards';


Vue.use(VueRouter)

const routes = [
	{
		path: '/admin',
		component: IndexAdmin,
		name: 'admin',
		redirect: {name: 'problemAdmin'},
		meta: {requiresAdmin: true},
		children: [
			{
				path: 'problems',
				component: ProblemsAdmin,
				name: 'problemAdmin',
				children: [
					{
						path: 'add',
						component: ProblemAdd,
						name: 'addNewProblem'
					},
					{
						path: ':id/edit',
						component: ProblemEdit,
						name: 'editProblem'
					}
				]
			},
			{
				path: 'categories',
				component: CategoriesAdmin,
				name: 'categoryAdmin',
				children: [
					{
						path: 'add',
						component: CategoryAdd,
						name: 'addNewCategory'
					}
				]
			},
			{
				path: 'users',
				component: UsersAdmin,
				name: 'userAdmin',
				children: [
					{
						path: 'add',
						component: UserAdd,
						name: 'addNewUser'
					},
					{
						path: ':id/edit',
						component: UserEdit,
						name: 'editUser'
					}
				]
			}
		]		
	},
	{
		path: '/',
		component: Index,
		meta: {requiresAuth: true},
		name: 'index'
	},
	{
		path: '/login',
		component: Login,
		meta: {questOnly: true},
		name: 'login'
	},
	{
		path: '/problems/:slug',
		component: SingleProblem,
		meta: {requiresAuth: true},
		name: 'singleProblem'
	},
	{
		path: '/profile',
		component: Profile,
		meta: {requiresAuth: true},
		name: 'profile',
		children: [
			{
				path: 'problems',
				component: ProfileProblems,
				meta: {requiresAuth: true},
				name: 'profileProblems'
			},
			{
				path: 'comments',
				component: ProfileComments,
				meta: {requiresAuth: true},
				name: 'profileComments'
			},
			{
				path: 'settings',
				component: ProfileSettings,
				meta: {requiresAuth: true},
				name: 'profileSettings'
			}
		]
	}
];

const router = new VueRouter({
    routes,
    mode: 'history'
});

router.beforeEach((to, from, next) => {
	Promise.resolve(to).then(requiresAuth).then(requiresAdmin).then(questOnly).then(() => {
			next();
	}).catch(redirect => {
			next(redirect);
	})
});

export default router;