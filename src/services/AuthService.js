import axios from 'axios';

import {myConst} from './../const/config.js';

export default class AuthService {
  constructor() {
    axios.defaults.baseURL = myConst.ENDPOINT_BASE_URL;
  }
  setAxiosDefaultAuthorizationHeader() {
    const TOKEN = 'Bearer ' + window.localStorage.getItem('ps_token');
    axios.defaults.headers.common['Authorization'] = TOKEN;
  }
  isAuthenticated() {
    console.log('login', !!window.localStorage.getItem('ps_token')); //eslint-disable-line
    return !!window.localStorage.getItem('ps_token');
  }
  isAdmin() {
    return !!window.localStorage.getItem('ps_role');
  }
  async login(email, password) {
    return axios.post('login', {email, password}).then((response) => {
      console.log(response); //eslint-disable-line
      window.localStorage.setItem('ps_token', response.data.token);
      window.localStorage.setItem('ps_user', response.data.user_id);
      if(response.data.user_role_id) {
        window.localStorage.setItem('ps_role', response.data.user_role_id);
      }
      this.setAxiosDefaultAuthorizationHeader();
    });
  }
  async refreshToken() {
    return axios.get('retoken').then(response => {
      console.log('auth re ', response) //eslint-disable-line
    });
  }
  logout() {
    window.localStorage.removeItem('ps_token');
    window.localStorage.removeItem('ps_user');
    if(window.localStorage.getItem('ps_role')) {
      window.localStorage.removeItem('ps_role');
    }
    delete axios.defaults.headers.common['Authorization'];
  }
}

export const authService = new AuthService();