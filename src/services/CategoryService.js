import axios from 'axios';

import {authService} from './AuthService';
import {myConst} from './../const/config.js';

export default class CategoryService {
  constructor() {
    axios.defaults.baseURL = myConst.ENDPOINT_BASE_URL;
  }
  getCategories() {
    authService.setAxiosDefaultAuthorizationHeader();
    return axios.get('categories');
  }
  addCategory(category) {
    authService.setAxiosDefaultAuthorizationHeader();
    return axios.post('categories', category);
  }
  editCategory(category, categoryId) {
    authService.setAxiosDefaultAuthorizationHeader();
    return axios.put(`categories/${categoryId}`, category);
  }
  deleteCategory(categoryId) {
    authService.setAxiosDefaultAuthorizationHeader();
    return axios.delete(`categories/${categoryId}`);
  }
}

export const categoryService = new CategoryService();