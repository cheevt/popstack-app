import axios from 'axios';

import {authService} from './AuthService';
import {myConst} from './../const/config.js';

export default class ProblemService {
  constructor() {
    axios.defaults.baseURL = myConst.ENDPOINT_BASE_URL;
  }
  getProblems(page) {
    authService.setAxiosDefaultAuthorizationHeader();
    return axios.get(`problems?page=${page}`);
  }
  getProblemById(problemId) {
    authService.setAxiosDefaultAuthorizationHeader();
    return axios.get(`problems/${problemId}`);
  }
  getProblemBySlug(slug) {
    authService.setAxiosDefaultAuthorizationHeader();
    return axios.get(`problems/${slug}`);
  }
  getProblemsByUser(userId) {
    authService.setAxiosDefaultAuthorizationHeader();
    return axios.get(`problems/user/${userId}`);
  }
  addProblem(problem) {
    authService.setAxiosDefaultAuthorizationHeader();
    return axios.post('problems', problem);
  }
  editProblem(problem, problemId) {
    authService.setAxiosDefaultAuthorizationHeader();
    return axios.put(`problems/${problemId}`, problem);
  }
  deleteProblem(problemId) {
    authService.setAxiosDefaultAuthorizationHeader();
    return axios.delete(`problems/${problemId}`);
  }
}

export const problemService = new ProblemService();