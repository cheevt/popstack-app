import axios from 'axios';

import { authService } from './AuthService';
import {myConst} from './../const/config.js';

export default class UserService {
  constructor() {
    axios.defaults.baseURL = myConst.ENDPOINT_BASE_URL;
  }
  getCurrentUser() {
    authService.setAxiosDefaultAuthorizationHeader();
    return axios.get('user');
  }
  getUserById(userId) {
    authService.setAxiosDefaultAuthorizationHeader();
    return axios.get(`users/${userId}`);
  }
  getUsers() {
    authService.setAxiosDefaultAuthorizationHeader();
    return axios.get('users');
  }
  addUser(user) {
    authService.setAxiosDefaultAuthorizationHeader();
    return axios.post('register', user);
  }
  editUser(user, userId) {
    console.log('USERRR', user); //eslint-disable-line
    authService.setAxiosDefaultAuthorizationHeader();
    return axios.put(`users/${userId}`, user);
  }
  deleteUser(userId) {
    authService.setAxiosDefaultAuthorizationHeader();
    return axios.delete(`users/${userId}`);
  }
}

export const userService = new UserService();