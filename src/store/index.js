import Vue from 'vue'
import Vuex from 'vuex'
import { authService } from '../services/AuthService';
import { categoryService } from '../services/CategoryService';

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    userLogged: false,
    isAdmin: false,
    currentUser: {},
    categories: []
  },
  getters: {
    getUserLogged(state) {
      return state.userLogged
    },
    getIsAdmin(state) {
      return state.isAdmin
    },
    getCurrentUser(state) {
      return state.currentUser
    },
    getCategories(state) {
      return state.categories
    }
  },
  mutations: {
    setUserLogged(state, userLogged){
      state.userLogged = userLogged
    },
    setIsAdmin(state, isAdmin) {
      state.isAdmin = isAdmin
    },
    setCurrentUser(state, currentUser) {
      state.currentUser = currentUser
    },
    setCategories(state, categories) {
      state.categories = categories
    },
    setCategory(state, category) {
      state.categories.push(category);
    },
    unsetCategory(state, category) {
      state.categories.splice(category, 1);
    },
    updateCategory(state, object) {
      console.log('cat ', object); //eslint-disable-line
      console.log('x', object.index); //eslint-disable-line
      //state.categories.splice(index, 1);
      state.categories.splice(object.index, 1, object.category);
    }
  },
  actions: {
    checkIsAdmin(state) {
      let check = authService.isAdmin()
      state.commit('setIsAdmin', check)
    },
    fetchCategories(state) {
      categoryService.getCategories().then(response => {
        console.log('RES', response); //eslint-disable-line
        state.commit('setCategories', response.data)
      }).catch(error => {
        console.log(error) //eslint-disable-line
      })
    }
  }
})